using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekAndWanderSteeringBehaviour : ArriveSteeringBehaviour
{
	bool steer = true;
	bool wander = false;

	public float WaypointDistance = 0.5f;
	public bool loop = false; // Won't use here, 

	public float WanderDistance = 2.0f;
	public float WanderRadius = 1.0f;
	public float WanderJitter = 20.0f;
	private Vector3 WanderTarget;
	private float circleRadius = 10.0f;

	private void Start() {
		float theta = (float)Random.value * Mathf.PI * 2;
		WanderTarget = new Vector3(WanderRadius * Mathf.Cos(theta),
								   0.0f,
								   WanderRadius * Mathf.Sin(theta));
	}

	public override void Init(SteeringAgent agent) {
		base.Init(agent);
		Vector2 randomPoint = Random.insideUnitCircle * circleRadius;
		target = new Vector3(randomPoint.x, 0f, randomPoint.y);
	}

	public override Vector3 calculateForce() {
		if (steer) {
			// When we arrive at target, switch to wander mode
			if ((target - steeringAgent.transform.position).magnitude < WaypointDistance) {
				steer = false;
				wander = true;
			}

			return CalculateArriveForce();
		} else {
			float jitterThisTimeSlice = WanderJitter * Time.deltaTime;

			WanderTarget = WanderTarget + new Vector3(Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice,
													  0.0f,
													  Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice);
			WanderTarget.Normalize();
			WanderTarget *= WanderRadius;

			target = WanderTarget + new Vector3(0, 0, WanderDistance);
			target = steeringAgent.transform.rotation * target + steeringAgent.transform.position;

			Vector3 wanderForce = (target - steeringAgent.transform.position).normalized;
			return wanderForce *= steeringAgent.maxSpeed;
		}
	}

	protected override void OnDrawGizmos() {
		base.OnDrawGizmos();

		if (steer) {
			Debug.DrawLine(transform.position, target, Color.black);
		} else {
			Vector3 circleCenter = transform.rotation * new Vector3(0, 0, WanderDistance) + transform.position;

			DebugExtension.DrawCircle(circleCenter, Vector3.up, Color.red, WanderRadius);
			Debug.DrawLine(transform.position, circleCenter, Color.yellow);
			Debug.DrawLine(transform.position, target, Color.blue);
		}
	}

    void OnTriggerEnter(Collider other) {
        if (other.tag == "OuterBound") {
			steer = true;
			wander = false;
			Vector2 randomPoint = Random.insideUnitCircle * circleRadius;
			target = new Vector3(randomPoint.x, 0f, randomPoint.y);
		}
    }
}
