using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathDrawGizmos : MonoBehaviour
{
    void OnDrawGizmos() {
        foreach (Transform child in transform) {
            DebugExtension.DebugWireSphere(child.position, Color.red);
        }
    }
}
