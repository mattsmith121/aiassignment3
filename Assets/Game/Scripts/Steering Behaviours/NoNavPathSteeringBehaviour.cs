using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NoNavPathSteeringBehaviour : ArriveSteeringBehaviour
{
	public float WaypointDistance = 0.5f;
	public bool loop = false; // Won't use here, 

	private int currentWaypointIndex = 0;

	public List<Transform> points;
	private int currentPoint = -1;

    public override void Init(SteeringAgent agent)
	{
		base.Init(agent);
		if (useMouseInput || points == null || points.Count == 0) {
			target = steeringAgent.transform.position;
		} else {
			GetNextPathPoint();
        }
	}

	public override Vector3 calculateForce()
	{
		if (useMouseInput) {
			checkMouseInput();
		}

		if (!useMouseInput && (target - steeringAgent.transform.position).magnitude < WaypointDistance)
		{
			GetNextPathPoint();
		}

		return CalculateArriveForce();
	}

	private void GetNextPathPoint() {
        currentPoint++;
        if (currentPoint >= points.Count) {
            if (loop) {
                currentPoint = 0;
            }
            else {
                return;
            }
        }
        target = points[currentPoint].position;
        newTarget = true;
    }

	protected override void OnDrawGizmos()
	{
		base.OnDrawGizmos();

		if (points != null)
		{
			for (int i = 1; i < points.Count; i++)
			{
				Debug.DrawLine(points[i - 1].position, points[i].position, Color.black);
			}
		}
	}
}
